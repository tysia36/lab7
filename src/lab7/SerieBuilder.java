package lab7;

import java.util.ArrayList;
import java.util.List;
import lab7.ChartSerie;
import lab7.Point;

public class SerieBuilder{
	
	private ChartSerie chartSerie;
	
	public SerieBuilder() {
        chartSerie = new ChartSerie();
    }
	
	public SerieBuilder addPoint(Point point)
	{
		List <Point> points = new ArrayList<Point>();
		points = chartSerie.getPoints();
		points.add(point);
		chartSerie.setPoints(points);
		return this;
	}
	public SerieBuilder  addLabel(String label)
	{
		chartSerie.setLabel(label);
		return this;
	}
	public SerieBuilder withPoints(List <Point> points)
	{
		chartSerie.setPoints(points);
		return this;
	}
	public SerieBuilder setType(SerieType type)
	{
		chartSerie.setSerieType(type);
		return this;
	}
	public ChartSerie build()
	{
		return chartSerie;
	}

}
