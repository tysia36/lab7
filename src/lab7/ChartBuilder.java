package lab7;

import java.util.ArrayList;
import java.util.List;
import lab7.ChartSettings;
import lab7.ChartSerie;

public class ChartBuilder
{
	private ChartSettings chartSettings;
	
	public ChartBuilder()
	{
		chartSettings = new ChartSettings();
	}
	
	public ChartBuilder addSerie (ChartSerie serie)
	{
		List <ChartSerie> series = new ArrayList<ChartSerie>();
		series=chartSettings.getSeries();
		series.add(serie);
		chartSettings.setSeries(series);
		return this;
	}
	public ChartBuilder withSeries(List<ChartSerie> series)
	{
		chartSettings.setSeries(series);
		return this;
	}
	public ChartBuilder withTitle(String title)
	{
		chartSettings.setTitle(title);
		return this;
	}
	public ChartBuilder withLegend()
	{
		chartSettings.setHaveLegend(true);
		return this;
	}
	public ChartBuilder withType (ChartType type)
	{
		chartSettings.setCharttype(type);
		return this;
	}
	public ChartSettings build()
	{
		return chartSettings;
	}
}
