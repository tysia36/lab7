package lab7;

import java.util.*;
import lab7.ChartSerie;
import lab7.ChartType;

public class ChartSettings 
{
	private List <ChartSerie> series = new ArrayList<ChartSerie>();
	private String title;
	private String subtitle;
	private boolean haveLegend;
	private ChartType charttype;
	
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public List<ChartSerie> getSeries() {
		return series;
	}
	public void setSeries(List<ChartSerie> series) {
		this.series = series;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean getHaveLegend() {
		return haveLegend;
	}
	public void setHaveLegend(boolean haveLegend) {
		this.haveLegend = haveLegend;
	}
	public ChartType getCharttype() {
		return charttype;
	}
	public void setCharttype(ChartType charttype) {
		this.charttype = charttype;
	}
	
	
}
