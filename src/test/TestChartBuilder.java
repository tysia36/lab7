package test;

import org.junit.*;
import lab7.ChartBuilder;
import lab7.ChartSerie;
import lab7.ChartSettings;
import lab7.ChartType;

import org.mockito.*;
import org.junit.runner.RunWith;   
import org.mockito.runners.MockitoJUnitRunner;  
import static org.assertj.core.api.Assertions.*;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestChartBuilder 
{
	
	@Mock
	ChartSerie serie1;
	@Mock
	ChartSerie serie2;
	
	@Mock
	List <ChartSerie> series;
	
	private ChartBuilder buildChart;
	
	@Before
    public void Before() {
        buildChart = new ChartBuilder();
    }
	
	@Test
	public void TestIfAddingWorks()
	{		
		//Given
		buildChart.addSerie(serie1).addSerie(serie2);
		//When
		ChartSettings settings = buildChart.build();
		//Then
		assertThat(settings.getSeries()).usingFieldByFieldElementComparator()
        .containsExactly(serie1, serie2);
	 }
	
	@Test
	public void TestIfSettingSeriesWorks()
	{
		//Given
		buildChart.withSeries(series);
		//When
		ChartSettings settings = buildChart.build();
		//Then
		assertThat(settings.getSeries()).isEqualTo(series);
	}
	
	@Test
	public void TestIfSettingTitleWorks ()
	{
		//Given
		buildChart.withTitle("Tytul");
		//When
		ChartSettings settings = buildChart.build();
		//Then
		assertThat(settings.getTitle()).isEqualTo("Tytul");
	}
	
	@Test
	public void TestIfSettingLegendWorks()
	{
		//Given
		buildChart.withLegend();
		//When
		ChartSettings settings = buildChart.build();
		//Then
		assertThat(settings.getHaveLegend()).isEqualTo(true);
	}
	
	@Test
	public void TestIfSettingTypeWorks()
	{
		//Given
		buildChart.withType(ChartType.Bar);
		//When
		ChartSettings settings = buildChart.build();
		//Then
		assertThat(settings.getCharttype()).isEqualTo(ChartType.Bar);
	}
}
