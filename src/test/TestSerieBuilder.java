package test;

import org.junit.*;
import lab7.ChartSerie;
import lab7.Point;
import lab7.SerieBuilder;
import lab7.SerieType;
import org.mockito.*;
import org.junit.runner.RunWith;   
import org.mockito.runners.MockitoJUnitRunner;  
import static org.assertj.core.api.Assertions.*;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestSerieBuilder 
{
	@Mock
	Point point1;
	@Mock
	Point point2;
	@Mock
	List <Point> points;
	
	private SerieBuilder buildSerie;
	
	@Before
    public void Before() 
	{
        buildSerie = new SerieBuilder();
    }
	
	@Test
	public void TestIfAddingWorks()
	{
		//Given
		buildSerie.addPoint(point1).addPoint(point2);
		//When
		ChartSerie serie = buildSerie.build();
		//Then
		assertThat(serie.getPoints()).usingFieldByFieldElementComparator()
		.containsExactly(point1, point2);
	 }
	
	@Test
	public void TestIfSettingPointsWorks()
	{
		//Given
		buildSerie.withPoints(points);
		//When
		ChartSerie serie = buildSerie.build();
		//Then
		assertThat(serie.getPoints()).isEqualTo(points);
	}
	
	@Test
	public void TestIfSettingLableWorks ()
	{
		//Given
		buildSerie.addLabel("Label");
		//When
		ChartSerie serie = buildSerie.build();
		//Then
		assertThat(serie.getLabel()).isEqualTo("Label");
	}
	
	@Test
	public void TestIfSettingTypeWorks()
	{
		//Given
		buildSerie.setType(SerieType.Bar);
		//When
		ChartSerie serie = buildSerie.build();
		//Then
		assertThat(serie.getSerieType()).isEqualTo(SerieType.Bar);
	}
}
